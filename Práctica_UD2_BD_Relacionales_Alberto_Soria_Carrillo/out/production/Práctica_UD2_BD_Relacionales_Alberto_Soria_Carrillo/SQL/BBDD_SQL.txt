create database optica2;
--
use optica2;
--
create table ojo_izq(
id int primary key auto_increment,
hipermetropia boolean,
astigmatismo boolean,
reducido boolean,
graduacion float,
tipo varchar(6)
);
--
create table ojo_drch(
id int primary key auto_increment,
hipermetropia boolean,
astigmatismo boolean,
reducido boolean,
graduacion float,
tipo varchar(6)
);
--
create table montura(
id int primary key auto_increment,
marca varchar(45),
modelo varchar(45),
precio float
);
--
create table cliente (
id int primary key auto_increment,
nombre varchar(45),
apellido1 varchar(45),
apellido2 varchar(45),
direccion varchar(65),
genero boolean,
fecha_nacimiento date
);
--
create table factura(
id int primary key auto_increment,
precio_total float,
fecha date,
name_optica varchar(45),
direccion varchar(45),
telefono varchar(45),
id_cliente int,
foreign key (id_cliente) references cliente(id)
);
--
create table factura_montura(
id int primary key auto_increment,
id_factura int,
id_montura int,
foreign key (id_factura) references factura(id),
foreign key (id_montura) references montura(id)
);
--
create table factura_ojo_izq(
id int primary key auto_increment,
id_factura int,
id_ojo_izq int,
foreign key (id_factura) references factura(id),
foreign key (id_ojo_izq) references ojo_izq(id));
--
create table factura_ojo_drch(
id int primary key auto_increment,
id_factura int,
id_ojo_drch int,
foreign key (id_factura) references factura(id),
foreign key (id_ojo_drch) references ojo_drch(id));
--
create procedure buscarCliente()
begin
	select * from cliente group by id;
end;
--
create procedure buscarOjoIzq()
begin
	select * from ojo_izq group by id;
end;
--
create procedure buscarOjoDrch()
begin
	select * from ojo_drch group by id;
end;
--
create procedure buscarMontura()
begin
	select * from montura group by id;
end;
--
create procedure buscarFactura()
begin
	select * from factura group by id;
end;
--
create procedure guardarOjoIzqFactura(id_factura2 int, id_ojo_izq2 int)
begin
	insert into factura_ojo_izq(id_factura,id_ojo_izq) value (id_factura2, id_ojo_izq2);
end;
--
create procedure guardarOjoDrchFactura(id_factura2 int, id_ojo_drch2 int)
begin
	insert into factura_ojo_drch(id_factura,id_ojo_drch) value (id_factura2, id_ojo_drch2);
end;
--
create procedure guardarFactura(precio_total2 float,fecha2 date, name_optica2 varchar(45), direccion2 varchar(45), telefono2 varchar(45), id_cliente2 int)
begin
	insert into factura(precio_total,fecha,name_optica,direccion,telefono,id_cliente)
    value(precio_total2, fecha2, name_optica2, direccion2 ,telefono2, id_cliente2);
end;
--
create procedure guardarMonturaFactura(id_factura2 int, id_montura2 int)
begin
	insert into factura_montura(id_factura,id_montura)
    value(id_factura2,idmontura2);
end;
--
insert into cliente(nombre, apellido1, apellido2, direccion, genero, fecha_nacimiento)
value ('Cliente1', 'Cliente1', 'Cliente1', 'Cliente1', false,'2021-01-13' );
--
insert into cliente(nombre, apellido1, apellido2, direccion, genero, fecha_nacimiento)
value ('Cliente2', 'Cliente2', 'Cliente2', 'Cliente2', false,'2021-01-13' );
--
insert into cliente(nombre, apellido1, apellido2, direccion, genero, fecha_nacimiento)
value ('Cliente3', 'Cliente3', 'Cliente3', 'Cliente3', false,'2021-01-13' );
--
insert into montura(marca,modelo,precio)
value ('Montura1','Montura1',55);
--
insert into montura(marca,modelo,precio)
value ('Montura2','Montura2',55);
--
insert into montura(marca,modelo,precio)
value ('Montura3','Montura3',55);
--
insert into ojo_izq(hipermetropia,astigmatismo,reducido,graduacion,tipo)
value (false,true,false,1,'Sol');
--
insert into ojo_izq(hipermetropia,astigmatismo,reducido,graduacion,tipo)
value (false,true,false,2,'Sol');
--
insert into ojo_izq(hipermetropia,astigmatismo,reducido,graduacion,tipo)
value (false,true,false,3,'Sol');
--
insert into ojo_drch(hipermetropia,astigmatismo,reducido,graduacion,tipo)
value (false,true,false,1,'Sol');
--
insert into ojo_drch(hipermetropia,astigmatismo,reducido,graduacion,tipo)
value (false,true,false,2,'Sol');
--
insert into ojo_drch(hipermetropia,astigmatismo,reducido,graduacion,tipo)
value (false,true,false,3,'Sol');