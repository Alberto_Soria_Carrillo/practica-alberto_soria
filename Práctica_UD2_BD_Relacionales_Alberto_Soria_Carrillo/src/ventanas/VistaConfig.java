package ventanas;

import datos.Datos;
import modeloControlador.Controlador;
import modeloControlador.Modelo;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.Properties;

/**
 * Clase VistaConfig. Ventana de configuración de los datos de conexión
 *
 * @author Alberto Soria Carrillo
 */
public class VistaConfig extends JFrame implements ActionListener {

    // Atributos
    private Datos dato; // Guarda los datos necesarios para el funcionamiento de la palicación
    private JPanel panel1;
    private JTextField ipServerTextField;
    private JTextField puertoServertextField2;
    private JTextField nombreBBDDTextField3;
    private JTextField nombreUsuarioTextField4;
    private JButton aceptarButton;
    private JButton cancelarButton;
    private JPasswordField contrasenaPasswordField1;

    // Constructor

    public VistaConfig(Datos dato) {
        this.dato = dato;
        createConfig(dato);
    }

    public VistaConfig() {

        this.dato = new Datos();

        // Controla si existe el archivo de configuración. En caso de que exista carga directamente la Ventana princípal
        if (!checkFileConfig()) {
            createConfig(dato);
        } else {
            extraerDatos();
            dato.setControlVistaConfig(false); // variable de control del boton cancelar de la ventana de configuración
            Vista vista = new Vista();
            Modelo modelo = new Modelo();
            Controlador controlador = new Controlador(dato, modelo, vista);
        }

    }

    /**
     * Extrae los datos del archivo propertis
     */
    private void extraerDatos() {
        try {
            FileInputStream file = new FileInputStream(dato.getConfig());

            Properties properties = new Properties();
            properties.load(file);

            dato.setIpServer(properties.getProperty("IP"));
            dato.setPuertoServer(properties.getProperty("PUERTO"));
            dato.setNombreBBDD(properties.getProperty("BBDD"));
            dato.setUsurarioBBDD(properties.getProperty("USUARIO"));
            dato.setPaswordUsuario(properties.getProperty("PASSWORD"));

            file.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            System.exit(0);
            e.printStackTrace();
        }
    }

    /**
     * Crea y añade los elementos necesarioa para la ventana
     *
     * @param dato
     */
    private void createConfig(Datos dato) {

        insertDataTextField(dato); // inserta los datos que tiene la palicación en los campos TextField
        setListener(this); // añade los ActionListener a los botones

        this.add(panel1);
        this.pack();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);

    }

    /**
     * Añade los ActionListener a los botones
     *
     * @param listener
     */
    private void setListener(ActionListener listener) {

        this.aceptarButton.addActionListener(listener);
        this.cancelarButton.addActionListener(listener);

    }

    /**
     * Inserta los datos guardados en la aplicacion en los campos TextField
     *
     * @param dato
     */
    private void insertDataTextField(Datos dato) {

        this.ipServerTextField.setText(dato.getIpServer());
        this.puertoServertextField2.setText(dato.getPuertoServer());
        this.nombreBBDDTextField3.setText(dato.getNombreBBDD());
        this.nombreUsuarioTextField4.setText(dato.getUsurarioBBDD());
        this.contrasenaPasswordField1.setText(dato.getPaswordUsuario());

    }

    /**
     * Comprueba la existencia del fichero de configuración
     *
     * @return
     */
    private boolean checkFileConfig() {
        File file = new File(dato.getConfig());
        return file.exists();
    }


    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == this.aceptarButton) {
            if (ipServerTextField.getText().equalsIgnoreCase("") || ipServerTextField.getText().isEmpty() ||
                    puertoServertextField2.getText().equalsIgnoreCase("") || puertoServertextField2.getText().isEmpty() ||
                    nombreBBDDTextField3.getText().equalsIgnoreCase("") || nombreBBDDTextField3.getText().isEmpty() ||
                    nombreUsuarioTextField4.getText().equalsIgnoreCase("") || nombreUsuarioTextField4.getText().isEmpty() ||
                    contrasenaPasswordField1.getText().equalsIgnoreCase("") || contrasenaPasswordField1.getText().isEmpty()) {

                JOptionPane.showMessageDialog(null, "Debes rellenar todos los campos");

            } else {
                saveData(); // guarda la informacion en la clase Datos
                createFile(); // crea el archivo properties
                if (dato.isControlVistaConfig() == true) {
                    dato.setControlVistaConfig(false); // variable de control del boton cancelar de la ventana de configuración
                    Vista vista = new Vista();
                    Modelo modelo = new Modelo();
                    Controlador controlador = new Controlador(dato, modelo, vista);
                }
                this.dispose();
            }

        }

        if (e.getSource() == this.cancelarButton) {

            // controla la accion que debe realizar el boton cancelar
            if (dato.isControlVistaConfig()) {
                System.exit(0);
            } else {
                this.dispose();
            }

        }

    }

    /**
     * crea el archivo properties
     */
    private void createFile() {

        try {

            PrintWriter destinoTXT = new PrintWriter(new FileWriter(dato.getConfig(), false));

            destinoTXT.println("# Nombre del servidor");
            destinoTXT.println("IP=" + dato.getIpServer());
            destinoTXT.println("# Puerto del mysql");
            destinoTXT.println("PUERTO=" + dato.getPuertoServer());
            destinoTXT.println("# Nombre de la base de datos");
            destinoTXT.println("BBDD=" + dato.getNombreBBDD());
            destinoTXT.println("# Usuario para la conexión");
            destinoTXT.println("USUARIO=" + dato.getUsurarioBBDD());
            destinoTXT.println("# Password para la conexión");
            destinoTXT.println("PASSWORD=" + dato.getPaswordUsuario());

            destinoTXT.close();

        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "Fallo en la creacion de " + dato.getConfig());
            e.printStackTrace();
        }


    }

    /**
     * Guarda la informacion en la clase Datos
     */
    private void saveData() {
        dato.setIpServer(ipServerTextField.getText().trim());
        dato.setPuertoServer(puertoServertextField2.getText().trim());
        dato.setNombreBBDD(nombreBBDDTextField3.getText().trim());
        dato.setUsurarioBBDD(nombreUsuarioTextField4.getText().trim());
        dato.setPaswordUsuario(contrasenaPasswordField1.getText().trim());
    }
}