package ventanas;

import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

/**
 * Clase Vista con la venta principal del programa
 * @author Alberto Soria Carrillo
 */
public class Vista extends JFrame {

    // Atributos
    private JPanel panel1;
    private JTabbedPane tabbedPane1;
    private JPanel facturaJPanel;
    private JPanel clienteJPanel;
    private JPanel monturaJPanel;
    private JPanel cristalJPanel;

    // Atributos Factura
    public JLabel nombreOpticaJLabel;
    public JLabel direcionOpticaJLabel;
    public JLabel telefonoJLabel;
    public JComboBox clienteFacturaComboBox;
    public JTextArea datosClienteFacturaTextArea;
    public JComboBox monturaFacturaComboBox;
    public JTable datosMonturaFacturaTable;
    public JComboBox ojoIzqFacturaComboBox;
    public JTable datosOjoIzqFacturaTable;
    public JComboBox ojoDrchFacturaComboBox;
    public JTable datosOjoDrchFacturaTable;
    public JButton generarFcturaButton;
    public JButton agregarMonturaFacturaButton;
    public JButton quitarMonturaFacturaButton;
    public JButton agregarOjoDrchFacturaButton;
    public JButton agregarOjoIzqFacturaButton;
    public JButton quitarOjoIzqFacturaButton;
    public JButton quitarOjoDrchButton;

    // Atributos Cliente
    public JTextField nombreClienteTextField;
    public JTextField apellido1TextField;
    public JTextField apellido2TextField;
    public JTextField dierccionClienteTextField;
    public JRadioButton mujerRadioButton;
    public JRadioButton hombreRadioButton;
    public JButton agrearClienteButton;
    public JButton buscarClienteButton;
    public JTable clienteTable;
    public JButton eliminarClienteButton;

    // Atributos Montura
    public JTextField marcaTextField;
    public JTextField modeloTextField;
    public JTextField precioTextField;
    public JButton agregarMonturaButton;
    public JButton buscarMonturaButton;
    public JTable monturaTable;
    public JButton eliminarMonturaButton;

    // Atributos Cristal
    // Atributos conjuntos
    public JButton agregarLosDosButton;
    public JButton buscarDiotriasEnLosButton;
    public JButton eliminarCristalButton;

    // Atributos Ojo Izquierdo
    public JCheckBox hipermetropiaCheckBox;
    public JCheckBox astigmatismoCheckBox;
    public JCheckBox reducidoCheckBox;
    public JTextField graduacionTextField;
    public JRadioButton normalRadioButton;
    public JRadioButton solRadioButton;
    public JButton agregarOjoIzquierdoButton;
    public JTable listaOjoIzquierdoTable;
    public JButton buscarDiotriasOjoIzquierdoButton;
    public JTextField buscarTextField;
    public JTable ojoIzquierdoTable;

    // Atributos Ojo Derecho
    public JCheckBox hipermetropiaCheckBox1;
    public JCheckBox astigmatismoCheckBox1;
    public JCheckBox reducidoCheckBox1;
    public JTextField graduacionTextField1;
    public JRadioButton normalRadioButton1;
    public JRadioButton solRadioButton1;
    public JButton agregarOjoDerechoButton;
    public JTable listaOjoDerechoTable;
    public JButton buscarDiotriasOjoDerechoButton;
    public JTextField buscarTextField1;
    public JTable ojoDerechoTable;
    public DatePicker fechaNacimientoCliente;



    // Atributos Adicionales
    private JMenuBar menuBar;
    private JMenu menu;
    public JMenuItem itemConectar;
    public JMenuItem itemConfiguracion;
    public JMenuItem itemSalir;
    public DefaultTableModel datosMonturaFacturaDtm;
    public DefaultTableModel datosOjoIzqFacturaDtm;
    public DefaultTableModel datosOjoDrchFacturaDtm;
    public DefaultTableModel clienteDtm;
    public DefaultTableModel monturaDtm;
    public DefaultTableModel listaOjoIzquierdoDtm;
    public DefaultTableModel listaOjoDerechoDtm;
    public DefaultTableModel ojoIzquierdoDtm;
    public DefaultTableModel ojoDerechoDtm;

    // Constructor
    public Vista() {

        this.add(panel1);

        startElement();
        createMenu();
        addTable();

        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);

    }

    /**
     * Agrega los DefaultTableModel a los JTablet
     */
    private void addTable() {

        datosMonturaFacturaTable.setModel(datosMonturaFacturaDtm);
        datosOjoIzqFacturaTable.setModel(datosOjoIzqFacturaDtm);
        datosOjoDrchFacturaTable.setModel(datosOjoDrchFacturaDtm);
        clienteTable.setModel(clienteDtm);
        monturaTable.setModel(monturaDtm);
        listaOjoIzquierdoTable.setModel(listaOjoIzquierdoDtm);
        listaOjoDerechoTable.setModel(listaOjoDerechoDtm);
        ojoIzquierdoTable.setModel(ojoIzquierdoDtm);
        ojoDerechoTable.setModel(ojoDerechoDtm);

    }

    /**
     * Crea el menu de la barra de herramientas
     */
    private void createMenu() {

        menu.add(itemConectar);
        menu.addSeparator();
        menu.add(itemConfiguracion);
        menu.add(itemSalir);
        menuBar.add(menu);
        this.setJMenuBar(menuBar);

    }

    /**
     * Inicializa los objetos de la clase Vista
     */
    private void startElement() {

        datosMonturaFacturaDtm = new DefaultTableModel();
        datosOjoIzqFacturaDtm = new DefaultTableModel();
        datosOjoDrchFacturaDtm = new DefaultTableModel();
        clienteDtm = new DefaultTableModel();
        monturaDtm = new DefaultTableModel();
        listaOjoIzquierdoDtm = new DefaultTableModel();
        listaOjoDerechoDtm = new DefaultTableModel();

        ojoIzquierdoDtm = new DefaultTableModel();
        ojoDerechoDtm = new DefaultTableModel();

        hombreRadioButton.setSelected(true);
        normalRadioButton.setSelected(true);
        normalRadioButton1.setSelected(true);

        menuBar = new JMenuBar();
        menu = new JMenu("Opciones");
        itemConectar = new JMenuItem("Conectar");
        itemConfiguracion = new JMenuItem("Configuracion");
        itemSalir = new JMenuItem("Salir");

    }

}
