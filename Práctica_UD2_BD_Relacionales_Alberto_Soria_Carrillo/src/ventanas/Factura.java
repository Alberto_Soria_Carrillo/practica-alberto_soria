package ventanas;

import com.itextpdf.text.*;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfWriter;
import conexion.Conexion;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.io.FileOutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * Clase Factura, ventana para visualizar y generar la factura
 */
public class Factura extends JFrame implements Printable, ActionListener {
    //Atributos
    private JPanel panel1;
    private JTextField nombreOpticaTextField;
    private JTextField direccionOpticaTextField;
    private JTextField telefonoOpticaTextField;
    private JTextField fechaOpticaTextField;
    private JTextField nombreClienteTextField;
    private JTextField idClienteTextField;
    private JTextArea descrpcionTextArea;
    private JButton imprimirButton;
    private JTextField precioTotalTextField;

    private Vista vista;
    private Conexion conexion;
    private String montura = "";
    private String ojo = "";
    private float monturaPrecio = 0;
    private float ojoPrecio = 0;
    private String[] cliente;
    // Constructor
    public Factura(Vista vista, Conexion conexion) {
        this.conexion = conexion;
        this.vista = vista;

        setActionListener(this);
        rellenarCampos(vista);

        this.add(panel1);
        this.pack();
        this.setVisible(true);


    }

    /**
     * Agrega el ActiveListener a los JButton
     * @param listener
     */
    private void setActionListener(ActionListener listener) {
        imprimirButton.addActionListener(listener);
    }

    /**
     * Rellena los campos de informacion de la ventana
     * @param vista
     */
    private void rellenarCampos(Vista vista) {

        nombreOpticaTextField.setText(vista.nombreOpticaJLabel.getText());
        direccionOpticaTextField.setText(vista.direcionOpticaJLabel.getText());
        telefonoOpticaTextField.setText(vista.telefonoJLabel.getText());
        fechaOpticaTextField.setText(String.valueOf(LocalDate.now()));

        cliente = vista.datosClienteFacturaTextArea.getText().split(" :~: ");

        nombreClienteTextField.setText(cliente[1]);
        idClienteTextField.setText(cliente[0]);

        datosMontura(vista.datosMonturaFacturaDtm);
        datosOjo(vista.datosOjoIzqFacturaDtm, "Izquierdo");
        datosOjo(vista.datosOjoDrchFacturaDtm, "Derecho");

        descrpcionTextArea.setText("Montura: " + monturaPrecio + "€\n" + montura + "\nCristales: " + ojoPrecio + "€\n" + ojo);
        precioTotalTextField.setText(String.valueOf((ojoPrecio + monturaPrecio) + "€"));

    }

    /**
     * Da formato a los datos de la tabla vista.datosMonturaFacturaDtm
     * @param dtm
     */
    private void datosMontura(DefaultTableModel dtm) {

        int contador = 0;

        try {
            while (dtm.getValueAt(contador, 0) != null) {
                String id = String.valueOf(dtm.getValueAt(contador, 0));
                String marca = String.valueOf(dtm.getValueAt(contador, 1));
                String modelo = String.valueOf(dtm.getValueAt(contador, 2));
                String precio = String.valueOf(dtm.getValueAt(contador, 3));

                montura = montura + "Codigo " + id + " marca: " + marca + ", modelo: " + modelo + ", precio: " + precio + "\n";

                monturaPrecio = monturaPrecio + Float.valueOf(precio);

                contador++;
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Extracción de los datos de la tabla Montura acabada");
        }

    }

    /**
     * Da formato a los datos de las tablas vista.datosOjoDrchFacturaDtm vista.datosOjoIzqFacturaDtm
     * @param dtm
     * @param lado
     */
    private void datosOjo(DefaultTableModel dtm, String lado) {
        int contador = 0;
        try {
            while (dtm.getValueAt(contador, 0) != null) {
                String id = String.valueOf(dtm.getValueAt(contador, 0));
                String hipermetropia = String.valueOf(dtm.getValueAt(contador, 1));
                String astigmatismo = String.valueOf(dtm.getValueAt(contador, 2));
                String reducido = String.valueOf(dtm.getValueAt(contador, 3));
                String graduacion = String.valueOf(dtm.getValueAt(contador, 4));
                String tipo = definirTipo(String.valueOf(dtm.getValueAt(contador, 5)));

                formatearRespuestaSiNo(hipermetropia);

                ojo = ojo + "Codigo " + id + ", ojo: " + lado + " hipermetropia " + formatearRespuestaSiNo(hipermetropia) + ", astigmatismo " + formatearRespuestaSiNo(astigmatismo) + ", reducido " + formatearRespuestaSiNo(reducido) + ", graduacion " + graduacion + ", tipo: " + tipo + "\n";

                ojoPrecio = ojoPrecio + calcularPrecioGraducion(Float.valueOf(graduacion));

                contador++;
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Extracción de los datos de la tabla Ojo " + lado + " acabada");
        }


    }

    /**
     * Resulelve la peticion de tipo del campo tipo de las tablas vista.datosOjoDrchFacturaDtm vista.datosOjoIzqFacturaDtm
     * @param valueOf
     * @return
     */
    private String definirTipo(String valueOf) {

        if (valueOf.equalsIgnoreCase("normal")) {
            return "normal";
        } else {
            return "sol";
        }

    }

    /**
     * Calcula el precio segun la graducion
     * @param valueOf
     * @return
     */
    private float calcularPrecioGraducion(Float valueOf) {

        return (valueOf * 30);

    }

    /**
     * Formate la respuesta de las columnas hipermetropia, astigmatismo y reducida de las tablas vista.datosOjoDrchFacturaDtm vista.datosOjoIzqFacturaDtm
     * @param s
     * @return
     */
    private String formatearRespuestaSiNo(String s) {

        if (s.equalsIgnoreCase("true")) {
            ojoPrecio = ojoPrecio + 50;
            return "si";
        } else {
            return "no";
        }

    }

    @Override
    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
        if (pageIndex == 0) {
            Graphics2D graphics2D = (Graphics2D) graphics;
            graphics2D.translate(pageFormat.getImageableX(), pageFormat.getImageableY());
            print(graphics2D);
            return PAGE_EXISTS;
        } else {
            return NO_SUCH_PAGE;
        }

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == imprimirButton) {

            Document document = new Document();
            try {

                String nombreArchivo = "Factura-" + cliente[0] + "-" + LocalDate.now();

                PdfWriter.getInstance(document, new FileOutputStream((nombreArchivo + ".pdf ")));
                // Para agregar imagenes al pdf
                // import com.itextpdf.text.Image;
                com.itextpdf.text.Image headerImg = com.itextpdf.text.Image.getInstance("ojo.jpg");
                // headerImg.scaleToFit(x,y); // Cambia el tamaño de la imagen
                headerImg.setAlignment(Chunk.ALIGN_CENTER); // dentra la imagen

                // Para agregar texto al documento
                Paragraph paragraph = new Paragraph(); // crea un objeto para poder introducir el texto
                paragraph.setAlignment(Paragraph.ALIGN_LEFT); // centra el contenido
                // Introduce el texto
                paragraph.add("Fecha de Facturacion: " + LocalDateTime.now() + "Datos de la óptica: \n\tNombre de la óptica: " + nombreOpticaTextField.getText() + "\n\tDirección: " + direccionOpticaTextField.getText() + "\n\tTélefono: " + telefonoOpticaTextField.getText() + "\n\n"
                        + "\n\n" + "Datos del cliente\n" + "\n\tNombre: " + cliente[1] + "\n\tCodigo id: " + cliente[0] + "\n\nDescripción\n\t" + descrpcionTextArea.getText() + "\n\n\tPrecio total: " + precioTotalTextField.getText());
                // Agrega fuente al texto
                paragraph.setFont(FontFactory.getFont("Times New Roman", 12, Font.NORMAL, BaseColor.BLACK));// se usa BaseColor para evitar errores

                document.open();
                document.add(headerImg);
                document.add(paragraph);
                document.close();

                createDataBaseRegister();

                JOptionPane.showMessageDialog(null, "Factura creada");

                this.dispose();

            } catch (Exception ex) {
                ex.printStackTrace();
                JOptionPane.showMessageDialog(null, "Fallo en la creacion de la factura");
                this.dispose();
            }

        }
    }

    /**
     * Crea las consultas y ejecuta las peticion en la BBDD para guardar la informacion de la factura
     * @throws SQLException
     */
    private void createDataBaseRegister() throws SQLException {

        String query = "call guardarFactura(" + precioTotalTextField.getText().replaceAll("€", "") + ", '" + fechaOpticaTextField.getText() + "', '" + nombreOpticaTextField.getText() + "', " +
                "'" + direccionOpticaTextField.getText() + "' ,'" + telefonoOpticaTextField.getText() + "', " + cliente[0] + ");";
        conexion.buscarPS(conexion, query);

        query = "select max(id) from factura;";
        ResultSet resultSet = conexion.buscarPS(conexion, query);

        int facturaID = 0;

        while (resultSet.next()) {

            facturaID = resultSet.getInt(1);
            System.out.println(facturaID + "   factura");
        }


        int contador = 0;
        try {
            while (vista.datosOjoIzqFacturaDtm.getValueAt(contador, 0) != null) {
                String id = String.valueOf(vista.datosOjoIzqFacturaDtm.getValueAt(contador, 0));

                query = "call guardarOjoIzqFactura(" + facturaID + "," + id + ")";
                conexion.buscarCS(conexion, query);
                contador++;
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Insercion tabla factura_ojo_izq realizada");
        }

        contador = 0;
        try {
            while (vista.datosOjoDrchFacturaDtm.getValueAt(contador, 0) != null) {
                String id = String.valueOf(vista.datosOjoDrchFacturaDtm.getValueAt(contador, 0));

                query = "call guardarOjoDrchFactura(" + facturaID + "," + id + ")";
                conexion.buscarCS(conexion, query);
                contador++;
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Insercion tabla factura_ojo_drch realizada");
        }

        contador = 0;
        try {
            while (vista.datosMonturaFacturaDtm.getValueAt(contador, 0) != null) {
                String id = String.valueOf(vista.datosMonturaFacturaDtm.getValueAt(contador, 0));

                query = "call guardarOjoDrchFactura(" + facturaID + "," + id + ")";
                conexion.buscarCS(conexion, query);
                contador++;
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Insercion tabla factuara_montura realizada");
        }

    }
}
