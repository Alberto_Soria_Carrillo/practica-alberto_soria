package modeloControlador;

import conexion.Conexion;
import titulosTablas.CabeceraCliente;
import titulosTablas.CabeceraMontura;
import titulosTablas.CabeceraOjo;
import titulosTablas.InfoOptica;
import ventanas.Vista;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Clase Modelo del MVC contiene los metoso usados por la clase Controlador
 *
 * @author Alberto Soria Carrillo
 */
public class Modelo {
    /**
     * Invoca los metodos para cargar los datos desde la BBDD
     *
     * @param vista
     * @param conexion
     * @throws SQLException
     */
    public void chargeDataBase(Vista vista, Conexion conexion) throws SQLException {

        chargeTableCliente(vista, conexion);
        chargeTableMontura(vista, conexion);
        chargeTableOjo(vista, conexion);

    }

    /**
     * Carga los metodos para cargar los datos de la BBDD correspondiente a las tablas ojo_drch y ojo_izq,
     * mediante los procedimientos buscarOjoIzq y buscarOjoDrch
     *
     * @param vista
     * @param conexion
     * @throws SQLException
     */
    public void chargeTableOjo(Vista vista, Conexion conexion) throws SQLException {

        String query = "call buscarOjoIzq";
        chargeTableOjo(vista.listaOjoIzquierdoDtm, vista.ojoIzqFacturaComboBox, conexion, query);
        query = "call buscarOjoDrch";
        chargeTableOjo(vista.listaOjoDerechoDtm, vista.ojoDrchFacturaComboBox, conexion, query);

    }

    /**
     * Carga los datos ontenidos de la BBDD en la tabla de la pestaña cristales que le corresponda, y los combobox
     * correspondientes al ojo izquierdo y derecho de la pestaña factura
     *
     * @param dtm
     * @param comboBox
     * @param conexion
     * @param query
     * @throws SQLException
     */
    private void chargeTableOjo(DefaultTableModel dtm, JComboBox comboBox, Conexion conexion, String query) throws SQLException {

        ResultSet resultSet = conexion.buscarCS(conexion, query);
        dtm.setRowCount(0);
        comboBox.removeAllItems();

        while (resultSet.next()) {
            String[] data = new String[6];

            data[0] = String.valueOf(resultSet.getInt(1));
            data[1] = String.valueOf(resultSet.getBoolean(2));
            data[2] = String.valueOf(resultSet.getBoolean(3));
            data[3] = String.valueOf(resultSet.getBoolean(4));
            data[4] = String.valueOf(resultSet.getFloat(5));
            data[5] = resultSet.getString(6);

            addDataDtm(dtm, data);

            String aux = data[0] + " " + data[4] + " " + data[5];
            addComboBox(comboBox, aux);
            comboBox.setSelectedIndex(-1);
        }

        resultSet.close();

    }

    /**
     * Carga los datos de la BBDD correspondientes a la tabla monturaDtm y
     * el combobox correspondiente a montura de la pestala factura
     *
     * @param vista
     * @param conexion
     * @throws SQLException
     */
    public void chargeTableMontura(Vista vista, Conexion conexion) throws SQLException {
        String query = "call buscarMontura";
        ResultSet resultSet = conexion.buscarCS(conexion, query);
        vista.monturaDtm.setRowCount(0);
        vista.monturaFacturaComboBox.removeAllItems();

        while (resultSet.next()) {
            String[] data = new String[4];
            rellenarDataMontura(resultSet, data);
            addDataDtm(vista.monturaDtm, data);

            String aux = data[0] + " " + data[1] + " " + data[2];
            addComboBox(vista.monturaFacturaComboBox, aux);
            vista.monturaFacturaComboBox.setSelectedIndex(-1);
        }
        resultSet.close();
    }

    /**
     * carga los datos de la BBDD correspondientes a la tabla clienteDtm y el combobox
     * correspondiente a cliente de la tabla factura
     *
     * @param vista
     * @param conexion
     * @throws SQLException
     */
    public void chargeTableCliente(Vista vista, Conexion conexion) throws SQLException {

        String query = "call buscarCliente()";
        ResultSet resultSet = conexion.buscarCS(conexion, query);
        vista.clienteDtm.setRowCount(0);
        vista.clienteFacturaComboBox.removeAllItems();

        String[] data = new String[7];
        while (resultSet.next()) {
            rellenarDataCliente(resultSet, data);
            addDataDtm(vista.clienteDtm, data);

            String aux = data[0] + " :~: " + data[1] + " " + data[2] + " " + data[3];
            addComboBox(vista.clienteFacturaComboBox, aux);
        }
        vista.clienteFacturaComboBox.setSelectedIndex(-1);
        resultSet.close();
    }

    /**
     * Agrega la informacion a la tabla indicada
     *
     * @param dtm
     * @param data
     */
    private void addDataDtm(DefaultTableModel dtm, String[] data) {
        dtm.addRow(data);
    }

    /**
     * Controla si se escoge entre hombre y mujer en la pestaña cliente
     *
     * @param aBoolean
     * @return
     */
    private String controlGenero(boolean aBoolean) {
        String aux = "";
        if (aBoolean == false) {
            aux = "Mujer";
        } else {
            aux = "hombre";
        }
        return aux;
    }

    /**
     * Agrega la información seleccionado al combobox correspondiente y lo posiciona en la posición -1
     *
     * @param comboBox
     * @param string
     */
    private void addComboBox(JComboBox comboBox, String string) {
        comboBox.setSelectedIndex(-1);
        comboBox.addItem(string);

    }

    /**
     * Realiza la peticion de datos para las cabeceras de las tablas e invoca los metodos para agregarlas
     *
     * @param vista
     */
    public void headerDefaultTableModelo(Vista vista) {

        CabeceraOjo headerEye[] = CabeceraOjo.values();
        CabeceraMontura headerSaddle[] = CabeceraMontura.values();
        CabeceraCliente headerClient[] = CabeceraCliente.values();

        headerCliente(vista, headerClient);
        headerMontura(vista, headerSaddle);
        headerOjo(vista, headerEye);
    }

    /**
     * Agrega las cabeceras a las tablas ojoIzquierdoDtm, ojoDerechoDtm, listaOjoDerechoDtm,
     * listaOjoIzquierdoDtm, datosOjoDrchFacturaDtm y datosOjoIzqFacturaDtm
     *
     * @param vista
     * @param headerEye
     */
    private void headerOjo(Vista vista, CabeceraOjo[] headerEye) {

        for (int i = 0; i < headerEye.length; i++) {
            vista.ojoIzquierdoDtm.addColumn(headerEye[i].getString());
            vista.ojoDerechoDtm.addColumn(headerEye[i].getString());
            vista.listaOjoDerechoDtm.addColumn(headerEye[i].getString());
            vista.listaOjoIzquierdoDtm.addColumn(headerEye[i].getString());
            vista.datosOjoDrchFacturaDtm.addColumn(headerEye[i].getString());
            vista.datosOjoIzqFacturaDtm.addColumn(headerEye[i].getString());
        }
    }

    /**
     * Agrega las cabeceras a las tablas .monturaDtm y datosMonturaFacturaDtm
     *
     * @param vista
     * @param headerSaddle
     */
    private void headerMontura(Vista vista, CabeceraMontura[] headerSaddle) {

        for (int i = 0; i < headerSaddle.length; i++) {
            vista.monturaDtm.addColumn(headerSaddle[i].getString());
            vista.datosMonturaFacturaDtm.addColumn(headerSaddle[i].getString());
        }
    }

    /**
     * Agrega las cabeceras a la tabla clienteDtm
     *
     * @param vista
     * @param headerClient
     */
    private void headerCliente(Vista vista, CabeceraCliente[] headerClient) {

        for (int i = 0; i < headerClient.length; i++) {
            vista.clienteDtm.addColumn(headerClient[i].getString());
        }

    }

    /**
     * Carga la información en los JLabel desde las clases enum
     *
     * @param vista
     */
    public void chargeJLabel(Vista vista) {

        InfoOptica[] infoOptica = InfoOptica.values();

        vista.nombreOpticaJLabel.setText(infoOptica[0].getString());
        vista.direcionOpticaJLabel.setText(infoOptica[1].getString());
        vista.telefonoJLabel.setText(infoOptica[2].getString());

    }

    /**
     * Comprueba si un campo JTextField esta en null y lo deja vacio. Evista problemas de excepción de punto nulo
     *
     * @param text
     * @return
     */
    private String comprobarCampo(JTextField text) {
        String aux = "";
        if (!text.getText().isEmpty()) {
            aux = text.getText();
        }
        return aux;
    }

    /**
     * Realiza la peticion para introducir un cliente en la BBDD
     *
     * @param vista
     * @param conexion
     * @throws SQLException
     */
    public void insertarCliente(Vista vista, Conexion conexion) throws SQLException {

        String[] auxfecha = formatFechaNacimiento(vista);

        String query = "insert into cliente(nombre, apellido1, apellido2, direccion, genero, fecha_nacimiento) " +
                "value ('" + vista.nombreClienteTextField.getText() + "', '" + vista.apellido1TextField.getText() + "', '" + vista.apellido2TextField.getText() + "', " +
                "'" + vista.dierccionClienteTextField.getText() + "', " + vista.hombreRadioButton.isSelected() + ", '" + auxfecha[0] + "');";

        conexion.insertarPS(conexion, query);
    }

    /**
     * Realiza la peticion para introducir una montura en la BBDD
     *
     * @param vista
     * @param conexion
     * @throws SQLException
     */
    public void insertarMontura(Vista vista, Conexion conexion) throws SQLException {

        String query = "insert into montura(marca,modelo,precio)" +
                "value('" + vista.marcaTextField.getText() + "','" + vista.modeloTextField.getText() + "'," + vista.precioTextField.getText() + ");";
        conexion.insertarPS(conexion, query);

    }

    /**
     * Realiza la peticion para insertar un cristal en la BBDD
     *
     * @param vista
     * @param conexion
     * @param ojo
     * @throws SQLException
     */
    public void insertarOjo(Vista vista, Conexion conexion, String ojo) throws SQLException {

        try {
            boolean hiper = definirBooleanHiper(vista, ojo);
            boolean astig = definirBooleanAstig(vista, ojo);
            boolean redu = definirBooleanRedu(vista, ojo);
            float graduacion = Float.valueOf(definirFloat(vista, ojo));
            String tipo = definirStringTipo(vista, ojo);
            ojo = definirStringOjo(vista, ojo);

            String query = "insert into " + ojo + "(hipermetropia,astigmatismo,reducido,graduacion,tipo)" +
                    "value(" + hiper + "," + astig + "," + redu + ", " + graduacion + ",'" + tipo + "');";
            conexion.insertarPS(conexion, query);
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "El campo graduación debe ser un numero");
        }
    }

    /**
     * Comprueba si se ha escogido "sol" o "normal" en la pestaña cristales
     *
     * @param vista
     * @param ojo
     * @return
     */
    private String definirStringTipo(Vista vista, String ojo) {
        if (ojo.equalsIgnoreCase(vista.agregarOjoIzquierdoButton.getText())) {
            if (vista.normalRadioButton.isSelected()) {
                return "Normal";
            } else {
                return "Sol";
            }
        } else {
            if (vista.normalRadioButton1.isSelected()) {
                return "Normal";
            } else {
                return "Sol";
            }
        }
    }

    /**
     * Controla en que tabla de la BBDD se debe guardar la informacion del cristal
     *
     * @param vista
     * @param ojo
     * @return
     */
    private String definirStringOjo(Vista vista, String ojo) {
        if (ojo.equalsIgnoreCase(vista.agregarOjoIzquierdoButton.getText())) {
            return "ojo_izq";
        } else {
            return "ojo_drch";
        }
    }

    /**
     * Cambia las comas por puntos antes del casteo de String a Float
     *
     * @param vista
     * @param ojo
     * @return
     */
    private String definirFloat(Vista vista, String ojo) {
        if (ojo.equalsIgnoreCase(vista.agregarOjoIzquierdoButton.getText())) {
            return vista.graduacionTextField.getText().replaceAll(",", ".");
        } else {
            return vista.graduacionTextField1.getText().replaceAll(",", ".");
        }
    }

    /**
     * Define si el campo "reducir" de la tabla cristales esta seleccionado
     *
     * @param vista
     * @param ojo
     * @return
     */
    private boolean definirBooleanRedu(Vista vista, String ojo) {
        if (ojo.equalsIgnoreCase(vista.agregarOjoIzquierdoButton.getText())) {
            return vista.reducidoCheckBox.isSelected();
        } else {
            return vista.reducidoCheckBox1.isSelected();
        }
    }

    /**
     * Define si el campo "astigmatismo" de la tabla cristales esta seleccionado
     *
     * @param vista
     * @param ojo
     * @return
     */
    private boolean definirBooleanAstig(Vista vista, String ojo) {
        if (ojo.equalsIgnoreCase(vista.agregarOjoIzquierdoButton.getText())) {
            return vista.astigmatismoCheckBox.isSelected();
        } else {
            return vista.astigmatismoCheckBox1.isSelected();
        }
    }

    /**
     * Define si el campo "hipermetropia" de la tabla cristales esta seleccionado
     *
     * @param vista
     * @param ojo
     * @return
     */
    private boolean definirBooleanHiper(Vista vista, String ojo) {
        if (ojo.equalsIgnoreCase(vista.agregarOjoIzquierdoButton.getText())) {
            return vista.hipermetropiaCheckBox.isSelected();
        } else {
            return vista.hipermetropiaCheckBox1.isSelected();
        }

    }

    /**
     * Formatea la el campo fecha
     *
     * @param vista
     * @return
     */
    private String[] formatFechaNacimiento(Vista vista) {
        return String.valueOf(vista.fechaNacimientoCliente.getDate()).split(" ");
    }

    /**
     * vacia los campos de la pestaña montura
     *
     * @param vista
     */
    public void borrarCamposMontura(Vista vista) {
        vista.marcaTextField.setText("");
        vista.modeloTextField.setText("");
        vista.precioTextField.setText("");
    }

    /**
     * Vacia los campos de la pestaña Clientes
     *
     * @param vista
     */
    public void borrarCamposCliente(Vista vista) {
        vista.nombreClienteTextField.setText("");
        vista.apellido1TextField.setText("");
        vista.apellido2TextField.setText("");
        vista.dierccionClienteTextField.setText("");
        vista.fechaNacimientoCliente.setText("");
    }

    /**
     * limpia los campos de la pestaña cristal
     *
     * @param vista
     */
    public void borrarCamposOjo(Vista vista) {
        vista.graduacionTextField.setText("");
        vista.graduacionTextField1.setText("");
        vista.buscarTextField.setText("");
        vista.buscarTextField1.setText("");
    }

    /**
     * Realiza la peticion de busqueda de la pestaña montura
     *
     * @param vista
     * @param conexion
     * @throws SQLException
     */
    public void buscarMontura(Vista vista, Conexion conexion) throws SQLException {
        String[] variablesArray = new String[3];

        variablesArray[0] = comprobarCampo(vista.marcaTextField);
        variablesArray[1] = comprobarCampo(vista.modeloTextField);
        variablesArray[2] = comprobarCampo(vista.precioTextField);

        String variable = "";

        for (int i = 0; i < variablesArray.length; i++) {

            if (!variablesArray[i].equalsIgnoreCase("")) {
                if (!variable.equals("")) {
                    variable = variable + " and ";
                }
                variable = variable + "'" + variablesArray[i] + "'" + whereMontura(variablesArray, i);
            }
        }
        ejecutarBusqueda(vista.monturaDtm, conexion, variable, "montura", 4, true);
    }

    /**
     * realiza la peticion de busqueda de la pestaña cliente
     *
     * @param vista
     * @param conexion
     * @throws SQLException
     */
    public void buscarCliente(Vista vista, Conexion conexion) throws SQLException {

        String[] variablesArray = new String[4];
        variablesArray[0] = comprobarCampo(vista.nombreClienteTextField);
        variablesArray[1] = comprobarCampo(vista.apellido1TextField);
        variablesArray[2] = comprobarCampo(vista.apellido2TextField);
        variablesArray[3] = comprobarCampo(vista.dierccionClienteTextField);

        String variable = "";

        for (int i = 0; i < variablesArray.length; i++) {

            if (!variablesArray[i].equalsIgnoreCase("")) {
                if (!variable.equals("")) {
                    variable = variable + " and ";
                }
                variable = variable + "'" + variablesArray[i] + "'" + whereCliente(variablesArray, i);
            }
        }
        ejecutarBusqueda(vista.clienteDtm, conexion, variable, "cliente", 7, true);
    }

    /**
     * realiza el dormateo del condicion de la busqueda en BBDD y realiza la ejecucion del metodo de busqueda
     *
     * @param variable
     * @param dtm
     * @param conexion
     * @param tabla
     * @throws SQLException
     */
    public void buscarOjo(String variable, DefaultTableModel dtm, Conexion conexion, String tabla) throws SQLException {

        variable = variable + " = graduacion ";
        ejecutarBusqueda(dtm, conexion, variable, tabla, 6, true);
    }

    /**
     * Crea la query de busqueda en la BBDD e invoca el metodo para realizar la busqueda e inserción de la información en la tabla correspocndiente
     *
     * @param dtm
     * @param conexion
     * @param variable
     * @param tabla
     * @param columnas
     * @param b
     * @throws SQLException
     */
    private void ejecutarBusqueda(DefaultTableModel dtm, Conexion conexion, String variable, String tabla, int columnas, boolean b) throws SQLException {

        if (b) {
            dtm.setRowCount(0);
        }
        String query = "select * from " + tabla + " where " + variable + " group by id";
        ResultSet resultSet = conexion.buscarPS(conexion, query);
        String[] data = new String[columnas];
        while (resultSet.next()) {
            if (tabla.equalsIgnoreCase("Cliente")) {
                rellenarDataCliente(resultSet, data);
            } else if (tabla.equalsIgnoreCase("Montura")) {
                rellenarDataMontura(resultSet, data);
            } else if (tabla.equalsIgnoreCase("ojo_drch")) {
                rellenarDataOjo(resultSet, data);
            } else if (tabla.equalsIgnoreCase("ojo_izq")) {
                rellenarDataOjo(resultSet, data);
            }
            addDataDtm(dtm, data);
        }
        resultSet.close();
    }

    /**
     * Rellena los campos de las tablas ojo
     * @param resultSet
     * @param data
     * @throws SQLException
     */
    private void rellenarDataOjo(ResultSet resultSet, String[] data) throws SQLException {

        data[0] = String.valueOf(resultSet.getInt(1));
        data[1] = String.valueOf(resultSet.getBoolean(2));
        data[2] = String.valueOf(resultSet.getBoolean(3));
        data[3] = String.valueOf(resultSet.getBoolean(4));
        data[4] = String.valueOf(resultSet.getFloat(5));
        data[5] = resultSet.getString(6);

    }

    /**
     * Rellena los campos de las tablas montura
     * @param resultSet
     * @param data
     * @throws SQLException
     */
    private void rellenarDataMontura(ResultSet resultSet, String[] data) throws SQLException {
        data[0] = String.valueOf(resultSet.getInt(1));
        data[1] = resultSet.getString(2);
        data[2] = resultSet.getString(3);
        data[3] = String.valueOf(resultSet.getInt(4));
    }

    /**
     * Rellena los datos de las tablas cliebte
     * @param resultSet
     * @param data
     * @throws SQLException
     */
    private void rellenarDataCliente(ResultSet resultSet, String[] data) throws SQLException {

        data[0] = String.valueOf(resultSet.getInt(1));
        data[1] = resultSet.getString(2);
        data[2] = resultSet.getString(3);
        data[3] = resultSet.getString(4);
        data[4] = resultSet.getString(5);
        data[5] = controlGenero(resultSet.getBoolean(6));
        String aux[] = String.valueOf(resultSet.getTimestamp(7)).split(" ");
        data[6] = aux[0];

    }

    /**
     * Rellena los datos de las tablas Montura
     * @param variablesArray
     * @param i
     * @return
     */
    private String whereMontura(String[] variablesArray, int i) {

        if (variablesArray[i].equals(variablesArray[0])) {
            return " = marca ";
        } else if (variablesArray[i].equals(variablesArray[1])) {
            return " = modelo ";
        } else if (variablesArray[i].equals(variablesArray[2])) {
            return " = precio ";
        }
        return "";
    }

    /**
     * Formatea el condicional del query para buscar clientes
     * @param variablesArray
     * @param i
     * @return
     */
    private String whereCliente(String[] variablesArray, int i) {
        if (variablesArray[i].equals(variablesArray[0])) {
            return " = nombre ";
        } else if (variablesArray[i].equals(variablesArray[1])) {
            return " = apellido1 ";
        } else if (variablesArray[i].equals(variablesArray[2])) {
            return " = apellido2 ";
        } else if (variablesArray[i].equals(variablesArray[3])) {
            return " = direccion ";
        }
        return "";
    }

    /**
     * Comprueba si el numero si el String introducido es un Float
     * @param text
     * @return
     */
    public boolean comprobarNumero(String text) {
        try {
            float aus = Float.valueOf(text);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Carga el cliente seleccionado en el campo datosClienteFacturaTextArea
     * @param vista
     */
    public void chargeClienteFacturaTextField(Vista vista) {
        String cliente = String.valueOf(vista.clienteFacturaComboBox.getItemAt(vista.clienteFacturaComboBox.getSelectedIndex()));
        if (!cliente.equals("null")) {
            vista.datosClienteFacturaTextArea.setText(cliente);
        } else {
            vista.datosClienteFacturaTextArea.setText("No selecionado");
        }
    }

    /**
     * carga el cliente seleccionado en la tabla datosMonturaFacturaDtm
     * @param dtm
     * @param id
     * @param conexion
     * @param table
     * @throws SQLException
     */
    public void chargeMonturaFacturaDtm(DefaultTableModel dtm, String id, Conexion conexion, String table) throws SQLException {

        String[] aux = id.split(" ");
        String variable = aux[0];
        if (!variable.equals("null")) {
            variable = variable + " = id";
            ejecutarBusqueda(dtm, conexion, variable, table, 7, false);
        }
    }

    /**
     * carga el cristal seleccionado en la tabla datosOjoFacturaDtm
     * @param dtm
     * @param id
     * @param conexion
     * @param table
     * @throws SQLException
     */
    public void chargeOjoFacturaDtm(DefaultTableModel dtm, String id, Conexion conexion, String table) throws SQLException {

        String[] aux = id.split(" ");
        String variable = aux[0];
        if (!variable.equals("null")) {
            variable = variable + " = id";
            ejecutarBusqueda(dtm, conexion, variable, table, 6, false);
        }

    }

    /**
     * Limpia todas las tablas
     * @param vista
     */
    public void clearTable(Vista vista) {

        vista.datosOjoIzqFacturaDtm.setRowCount(0);
        vista.datosOjoDrchFacturaDtm.setRowCount(0);
        vista.datosMonturaFacturaDtm.setRowCount(0);
        vista.monturaDtm.setRowCount(0);
        vista.clienteDtm.setRowCount(0);
        vista.datosOjoDrchFacturaDtm.setRowCount(0);
        vista.datosMonturaFacturaDtm.setRowCount(0);
        vista.datosOjoIzqFacturaDtm.setRowCount(0);
        vista.listaOjoIzquierdoDtm.setRowCount(0);
        vista.listaOjoDerechoDtm.setRowCount(0);
        vista.ojoIzquierdoDtm.setRowCount(0);
        vista.ojoDerechoDtm.setRowCount(0);
    }

    /**
     * Limpia todos los combobox
     * @param vista
     */
    public void clearComboBox(Vista vista) {

        vista.monturaFacturaComboBox.removeAllItems();
        vista.clienteFacturaComboBox.removeAllItems();
        vista.ojoDrchFacturaComboBox.removeAllItems();
        vista.ojoIzqFacturaComboBox.removeAllItems();

    }

    /**
     * realiza la ejecución de la query de eliminación
     * @param id
     * @param conexion
     * @param tabla
     * @throws SQLException
     */
    public void remove(String id, Conexion conexion, String tabla) throws SQLException {

        String query = "delete from " + tabla + " where id=" + id;
        conexion.deletPS(query, conexion);

    }

    /**
     * Invoca el metodo para crear la query de actualización y la ejecuta
     * @param dtm
     * @param filaModicada
     * @param tabla
     * @param conexion
     * @throws SQLException
     */
    public void updateRegister(DefaultTableModel dtm, int filaModicada, String tabla, Conexion conexion) throws SQLException {
        String query = formatQuery(dtm, filaModicada, tabla);
        conexion.updatePS(query, conexion);
    }

    /**
     * Crea la query de actualización
     * @param dtm
     * @param filaModicada
     * @param tabla
     * @return
     */
    private String formatQuery(DefaultTableModel dtm, int filaModicada, String tabla) {

        String query = "";
        String id;
        String nombre;
        String apellido1;
        String apellido2;
        String direccion;
        boolean genero;
        String fecha;
        String marca;
        String modelo;
        String precio;
        String hipermetropia;
        String astigmatismo;
        String reducido;
        float graduacion;
        String tipo;


        switch (tabla) {
            case "cliente":
                id = String.valueOf(dtm.getValueAt(filaModicada, 0));
                nombre = String.valueOf(dtm.getValueAt(filaModicada, 1));
                apellido1 = String.valueOf(dtm.getValueAt(filaModicada, 2));
                apellido2 = String.valueOf(dtm.getValueAt(filaModicada, 3));
                direccion = String.valueOf(dtm.getValueAt(filaModicada, 4));
                genero = String.valueOf(dtm.getValueAt(filaModicada, 5)).equalsIgnoreCase("Hombre");
                fecha = String.valueOf(dtm.getValueAt(filaModicada, 6));

                query = "update " + tabla + " set nombre='" + nombre + "', apellido1='" + apellido1 + "', apellido2='" + apellido2 + "', direccion='" + direccion + "', genero=" + genero + ", fecha_nacimiento='" + fecha + "' where id=" + id + ";";

                break;
            case "montura":
                id = String.valueOf(dtm.getValueAt(filaModicada, 0));
                marca = String.valueOf(dtm.getValueAt(filaModicada, 1));
                modelo = String.valueOf(dtm.getValueAt(filaModicada, 2));
                precio = String.valueOf(dtm.getValueAt(filaModicada, 3));

                query = "update " + tabla + " set marca='" + marca + "', modelo='" + modelo + "', precio=" + precio + " where id=" + id + ";";

                break;
            case "ojo_drch":
            case "ojo_izq":
                id = String.valueOf(dtm.getValueAt(filaModicada, 0));
                hipermetropia = String.valueOf(dtm.getValueAt(filaModicada, 1));
                astigmatismo = String.valueOf(dtm.getValueAt(filaModicada, 2));
                reducido = String.valueOf(dtm.getValueAt(filaModicada, 3));
                graduacion = Float.valueOf(String.valueOf(dtm.getValueAt(filaModicada, 4)));
                tipo = String.valueOf(dtm.getValueAt(filaModicada, 5));

                query = "update " + tabla + " set hipermetropia=" + hipermetropia + ", astigmatismo=" + astigmatismo + ", reducido=" + reducido + ", graduacion=" + graduacion + ", tipo='" + tipo + "' where id=" + id + ";";

                break;
        }

        return query;

    }
}