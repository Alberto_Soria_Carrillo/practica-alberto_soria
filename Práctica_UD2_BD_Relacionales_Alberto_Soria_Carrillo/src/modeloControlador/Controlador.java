package modeloControlador;

import conexion.Conexion;
import datos.Datos;
import ventanas.Factura;
import ventanas.Vista;
import ventanas.VistaConfig;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.SQLException;

/**
 * Clase Controlador del MVC, carga la venta y se ocupa de realizar el enlace entre los diferentes elementos
 *
 * @author Alberto Soria Carrillo
 */
public class Controlador implements ActionListener, TableModelListener {

    private Datos datos;
    private Modelo modelo;
    private Vista vista;
    private Conexion conexion;

    // Controlador
    public Controlador(Datos datos, Modelo modelo, Vista vista) {

        this.datos = datos;
        this.modelo = modelo;
        this.vista = vista;
        this.conexion = new Conexion(datos);

        modelo.headerDefaultTableModelo(vista);
        modelo.chargeJLabel(vista);

        setActionListener(this);
        setTableModelListener(this);

    }

    /**
     * Agrega los TableModelListener a las tablas
     * @param listener
     */
    private void setTableModelListener(TableModelListener listener) {

        // Factura
        vista.datosMonturaFacturaDtm.addTableModelListener(listener);
        vista.datosOjoIzqFacturaDtm.addTableModelListener(listener);
        vista.datosOjoDrchFacturaDtm.addTableModelListener(listener);

        // Cliente
        vista.clienteDtm.addTableModelListener(listener);

        // Montura
        vista.monturaDtm.addTableModelListener(listener);

        // Cristal
        vista.datosOjoIzqFacturaDtm.addTableModelListener(listener);
        vista.datosOjoDrchFacturaDtm.addTableModelListener(listener);
        vista.ojoDerechoDtm.addTableModelListener(listener);
        vista.ojoIzquierdoDtm.addTableModelListener(listener);

    }

    /**
     * Agrega los ActionListener
     * @param listener
     */
    private void setActionListener(ActionListener listener) {

        // Menu
        vista.itemConectar.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
        vista.itemConfiguracion.addActionListener(listener);

        // Factura
        vista.clienteFacturaComboBox.addActionListener(listener);
        vista.monturaFacturaComboBox.addActionListener(listener);
        vista.ojoIzqFacturaComboBox.addActionListener(listener);
        vista.ojoDrchFacturaComboBox.addActionListener(listener);
        vista.generarFcturaButton.addActionListener(listener);
        vista.agregarMonturaFacturaButton.addActionListener(listener);
        vista.quitarMonturaFacturaButton.addActionListener(listener);
        vista.agregarOjoDrchFacturaButton.addActionListener(listener);
        vista.agregarOjoIzqFacturaButton.addActionListener(listener);
        vista.quitarOjoIzqFacturaButton.addActionListener(listener);
        vista.quitarOjoDrchButton.addActionListener(listener);

        // Cliente
        vista.agrearClienteButton.addActionListener(listener);
        vista.buscarClienteButton.addActionListener(listener);
        vista.eliminarClienteButton.addActionListener(listener);

        // Montura
        vista.agregarMonturaButton.addActionListener(listener);
        vista.buscarMonturaButton.addActionListener(listener);
        vista.eliminarMonturaButton.addActionListener(listener);

        // Cristal
        vista.agregarOjoIzquierdoButton.addActionListener(listener);
        vista.agregarOjoDerechoButton.addActionListener(listener);
        vista.agregarLosDosButton.addActionListener(listener);
        vista.buscarDiotriasOjoIzquierdoButton.addActionListener(listener);
        vista.buscarDiotriasOjoDerechoButton.addActionListener(listener);
        vista.buscarDiotriasEnLosButton.addActionListener(listener);
        vista.eliminarCristalButton.addActionListener(listener);

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        // Menu
        if (e.getSource() == vista.itemSalir) {
            if (conexion.getConnection() != null) {
                conexion.desconectar();
            }
            System.exit(0);
        }
        if (e.getSource() == vista.itemConfiguracion) {
            VistaConfig vistaConfig = new VistaConfig(datos);
        }
        if (e.getSource() == vista.itemConectar) {
            if (conexion.getConnection() == null) {
                conexion.conectar();

            } else {
                conexion.desconectar();
                conexion.setConnection(null);
                modelo.clearTable(vista);
                modelo.clearComboBox(vista);
            }
            if (conexion.getConnection() != null) {
                vista.itemConectar.setText("Desconectar");
                try {
                    modelo.chargeDataBase(vista, conexion);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
            if (conexion.getConnection() == null) {
                vista.itemConectar.setText("Conectar");
            }
        }

        if (conexion.getConnection() != null) {
            // Factura
            if (e.getSource() == vista.clienteFacturaComboBox) {
                modelo.chargeClienteFacturaTextField(vista);
            }
            if (e.getSource() == vista.agregarMonturaFacturaButton) {
                try {
                    String montura = String.valueOf(vista.monturaFacturaComboBox.getItemAt(vista.monturaFacturaComboBox.getSelectedIndex()));
                    modelo.chargeMonturaFacturaDtm(vista.datosMonturaFacturaDtm, montura, conexion, "montura");
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
            if (e.getSource() == vista.agregarOjoIzqFacturaButton) {
                try {
                    String ojo = String.valueOf(vista.ojoIzqFacturaComboBox.getItemAt(vista.ojoIzqFacturaComboBox.getSelectedIndex()));
                    modelo.chargeOjoFacturaDtm(vista.datosOjoIzqFacturaDtm, ojo, conexion, "ojo_izq");
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
            if (e.getSource() == vista.agregarOjoDrchFacturaButton) {
                try {
                    String ojo = String.valueOf(vista.ojoDrchFacturaComboBox.getItemAt(vista.ojoDrchFacturaComboBox.getSelectedIndex()));
                    modelo.chargeOjoFacturaDtm(vista.datosOjoDrchFacturaDtm, ojo, conexion, "ojo_drch");
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }

            if (e.getSource() == vista.quitarMonturaFacturaButton) {
                try {
                    vista.datosMonturaFacturaDtm.removeRow(vista.datosMonturaFacturaTable.getSelectedRow());
                } catch (ArrayIndexOutOfBoundsException e1) {

                }
            }
            if (e.getSource() == vista.quitarOjoIzqFacturaButton) {
                try {
                    vista.datosOjoIzqFacturaDtm.removeRow(vista.datosOjoIzqFacturaTable.getSelectedRow());
                } catch (ArrayIndexOutOfBoundsException e1) {

                }
            }
            if (e.getSource() == vista.quitarOjoDrchButton) {
                try {
                    vista.datosOjoDrchFacturaDtm.removeRow(vista.datosOjoDrchFacturaTable.getSelectedRow());
                } catch (ArrayIndexOutOfBoundsException e1) {

                }
            }
            if (e.getSource() == vista.generarFcturaButton) {


                if (vista.datosClienteFacturaTextArea.getText().indexOf(" :~: ") != -1) {
                    Factura factura = new Factura(vista, conexion);
                } else {
                    JOptionPane.showMessageDialog(null, "Tiene que escoger un cliente");
                }


            }

            // Cliente
            if (e.getSource() == vista.buscarClienteButton) {
                if ((vista.dierccionClienteTextField.getText().equals("") || vista.dierccionClienteTextField.getText().isEmpty()) &&
                        (vista.nombreClienteTextField.getText().equals("") || vista.nombreClienteTextField.getText().isEmpty()) &&
                        (vista.apellido1TextField.getText().equals("") || vista.apellido1TextField.getText().isEmpty()) &&
                        (vista.apellido1TextField.getText().equals("") || vista.apellido1TextField.getText().isEmpty())) {
                    JOptionPane.showMessageDialog(null, "Debes rellenar al menos un campo: \nNombre, 1º Apellido, 2º Apellido o Direccion");
                } else {
                    try {
                        modelo.buscarCliente(vista, conexion);
                        modelo.borrarCamposCliente(vista);
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }
                }
            }
            if (e.getSource() == vista.agrearClienteButton) {
                if ((vista.dierccionClienteTextField.getText().equals("") || vista.dierccionClienteTextField.getText().isEmpty()) ||
                        (vista.nombreClienteTextField.getText().equals("") || vista.nombreClienteTextField.getText().isEmpty()) ||
                        (vista.apellido1TextField.getText().equals("") || vista.apellido1TextField.getText().isEmpty()) ||
                        (vista.apellido1TextField.getText().equals("") || vista.apellido1TextField.getText().isEmpty()) ||
                        (vista.fechaNacimientoCliente.getText().equals("") || vista.fechaNacimientoCliente.getText().isEmpty())
                ) {
                    JOptionPane.showMessageDialog(null, "Debes rellena todos los campos");
                } else {
                    try {
                        modelo.insertarCliente(vista, conexion);
                        modelo.borrarCamposCliente(vista);
                        modelo.chargeTableCliente(vista, conexion);
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }
                }
            }
            if (e.getSource() == vista.eliminarClienteButton) {
                try {
                    int fila = vista.clienteTable.getSelectedRow();
                    String id = (String) vista.clienteDtm.getValueAt(fila, 0);
                    modelo.remove(id, conexion, "cliente");
                    modelo.clearTable(vista);
                    modelo.clearComboBox(vista);
                    modelo.chargeDataBase(vista, conexion);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                } catch (ArrayIndexOutOfBoundsException ex) {
                    JOptionPane.showMessageDialog(null, "Debes seleccionar un registro");
                }
            }

            // Montura
            if (e.getSource() == vista.buscarMonturaButton) {
                if ((vista.marcaTextField.getText().equals("") || vista.marcaTextField.getText().isEmpty()) &&
                        (vista.modeloTextField.getText().equals("") || vista.modeloTextField.getText().isEmpty()) &&
                        (vista.precioTextField.getText().equals("") || vista.precioTextField.getText().isEmpty())) {
                    JOptionPane.showMessageDialog(null, "Debes rellenar al menos un campo");
                } else {
                    try {
                        if (modelo.comprobarNumero(vista.precioTextField.getText())) {
                            modelo.buscarMontura(vista, conexion);
                        } else {
                            JOptionPane.showMessageDialog(null, "El campo precio debe ser un numero");
                        }
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }
                }
            }
            if (e.getSource() == vista.agregarMonturaButton) {

                try {
                    modelo.insertarMontura(vista, conexion);
                    modelo.borrarCamposMontura(vista);
                    modelo.chargeTableMontura(vista, conexion);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }

            }
            if (e.getSource() == vista.eliminarMonturaButton) {
                try {
                    int fila = vista.monturaTable.getSelectedRow();
                    String id = (String) vista.monturaDtm.getValueAt(fila, 0);
                    modelo.remove(id, conexion, "montura");
                    modelo.clearTable(vista);
                    modelo.clearComboBox(vista);
                    modelo.chargeDataBase(vista, conexion);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                } catch (ArrayIndexOutOfBoundsException ex) {
                    JOptionPane.showMessageDialog(null, "Debes seleccionar un registro");
                }

            }

            // critsales

            if (e.getSource() == vista.agregarOjoIzquierdoButton) {
                if (!vista.graduacionTextField.getText().equalsIgnoreCase("") && !vista.graduacionTextField.getText().isEmpty()) {
                    try {
                        modelo.insertarOjo(vista, conexion, vista.agregarOjoIzquierdoButton.getText());
                        modelo.borrarCamposOjo(vista);
                        modelo.chargeTableOjo(vista, conexion);
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Debe indicar la graduación");
                }
            }
            if (e.getSource() == vista.agregarOjoDerechoButton) {
                if (!vista.graduacionTextField1.getText().equalsIgnoreCase("") && !vista.graduacionTextField1.getText().isEmpty()) {
                    try {
                        modelo.insertarOjo(vista, conexion, vista.agregarOjoDerechoButton.getText());
                        modelo.borrarCamposOjo(vista);
                        modelo.chargeTableOjo(vista, conexion);
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Debe indicar la graduación");
                }
            }
            if (e.getSource() == vista.agregarLosDosButton) {
                if (!vista.graduacionTextField.getText().equalsIgnoreCase("") && !vista.graduacionTextField.getText().isEmpty() &&
                        !vista.graduacionTextField1.getText().equalsIgnoreCase("") && !vista.graduacionTextField1.getText().isEmpty()) {
                    try {
                        modelo.insertarOjo(vista, conexion, vista.agregarOjoIzquierdoButton.getText());
                        modelo.insertarOjo(vista, conexion, vista.agregarOjoDerechoButton.getText());
                        modelo.borrarCamposOjo(vista);
                        modelo.chargeTableOjo(vista, conexion);
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }
                }
            }
            if (e.getSource() == vista.buscarDiotriasOjoIzquierdoButton) {

                if (!vista.buscarTextField.getText().equals("") || !vista.buscarTextField.getText().isEmpty()) {
                    if (modelo.comprobarNumero(vista.buscarTextField.getText())) {
                        try {
                            modelo.buscarOjo(vista.buscarTextField.getText(), vista.ojoIzquierdoDtm, conexion, "ojo_izq");
                        } catch (SQLException ex) {
                            ex.printStackTrace();
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "La graduación debe ser un número");
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Debe indicar la graduación");
                }

            }
            if (e.getSource() == vista.buscarDiotriasOjoDerechoButton) {

                if (!vista.buscarTextField1.getText().equals("") || !vista.buscarTextField1.getText().isEmpty()) {
                    if (modelo.comprobarNumero(vista.buscarTextField1.getText())) {
                        try {
                            modelo.buscarOjo(vista.buscarTextField1.getText(), vista.ojoDerechoDtm, conexion, "ojo_drch");
                        } catch (SQLException ex) {
                            ex.printStackTrace();
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "La graduación debe ser un número");
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Debe indicar la graduación");
                }


            }
            if (e.getSource() == vista.buscarDiotriasEnLosButton) {

                if ((!vista.buscarTextField1.getText().equals("") || !vista.buscarTextField1.getText().isEmpty() &&
                        (!vista.buscarTextField.getText().equals("") || !vista.buscarTextField.getText().isEmpty()))) {
                    if (modelo.comprobarNumero(vista.buscarTextField1.getText())) {
                        try {
                            modelo.buscarOjo(vista.buscarTextField1.getText(), vista.ojoDerechoDtm, conexion, "ojo_drch");
                            modelo.buscarOjo(vista.buscarTextField.getText(), vista.ojoIzquierdoDtm, conexion, "ojo_izq");
                        } catch (SQLException ex) {
                            ex.printStackTrace();
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "La graduación debe ser un número");
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Debe indicar la graduación");
                }

            }
            if (e.getSource() == vista.eliminarCristalButton) {

                try {
                    int filaIzq = vista.listaOjoIzquierdoTable.getSelectedRow();
                    if (filaIzq != -1) {
                        String id = (String) vista.listaOjoIzquierdoDtm.getValueAt(filaIzq, 0);
                        modelo.remove(id, conexion, "ojo_izq");
                    }

                    int filaIzq2 = vista.ojoIzquierdoTable.getSelectedRow();
                    if (filaIzq2 != -1) {
                        String id = (String) vista.ojoIzquierdoDtm.getValueAt(filaIzq2, 0);
                        modelo.remove(id, conexion, "ojo_izq");
                    }

                    int filaDrch = vista.listaOjoDerechoTable.getSelectedRow();
                    if (filaDrch != -1) {
                        String id = (String) vista.listaOjoDerechoDtm.getValueAt(filaDrch, 0);
                        modelo.remove(id, conexion, "ojo_drch");
                    }

                    int filaDrch2 = vista.ojoDerechoTable.getSelectedRow();
                    if (filaDrch2 != -1) {
                        String id = (String) vista.ojoDerechoDtm.getValueAt(filaDrch2, 0);
                        modelo.remove(id, conexion, "ojo_drch");
                    }

                    if (filaDrch == -1 && filaIzq == -1 && filaIzq2 == -1 && filaDrch2 == -1) {
                        JOptionPane.showMessageDialog(null, "Debes seleccionar un registro");
                    }

                    modelo.clearTable(vista);
                    modelo.clearComboBox(vista);
                    modelo.chargeDataBase(vista, conexion);

                } catch (SQLException ex) {
                    ex.printStackTrace();
                } catch (ArrayIndexOutOfBoundsException ex) {
                    JOptionPane.showMessageDialog(null, "Debes seleccionar un registro");
                }

            }

        }

    }

    @Override
    public void tableChanged(TableModelEvent e) {
        if (e.getType() == TableModelEvent.UPDATE) {
            int filaModicada = e.getFirstRow();
            try {
                if (e.getSource() == vista.clienteDtm) {
                    modelo.updateRegister(vista.clienteDtm, filaModicada, "cliente", conexion);
                } else if (e.getSource() == vista.monturaDtm) {
                    modelo.updateRegister(vista.monturaDtm, filaModicada, "montura", conexion);
                } else if (e.getSource() == vista.listaOjoIzquierdoDtm || e.getSource() == vista.ojoIzquierdoDtm) {
                    if (e.getSource() == vista.listaOjoIzquierdoDtm) {
                        modelo.updateRegister(vista.listaOjoIzquierdoDtm, filaModicada, "ojo_izq", conexion);
                    } else {
                        modelo.updateRegister(vista.ojoIzquierdoDtm, filaModicada, "ojo_izq", conexion);
                    }
                } else if (e.getSource() == vista.listaOjoDerechoDtm || e.getSource() == vista.ojoDerechoDtm) {
                    if (e.getSource() == vista.listaOjoDerechoDtm) {
                        modelo.updateRegister(vista.listaOjoDerechoDtm, filaModicada, "ojo_drch", conexion);
                    } else {
                        modelo.updateRegister(vista.ojoDerechoDtm, filaModicada, "ojo_drch", conexion);
                    }
                }

            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Formato de los datos incorrecto");
            }
            modelo.clearTable(vista);
            modelo.clearComboBox(vista);
            try {
                modelo.chargeDataBase(vista, conexion);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
}
