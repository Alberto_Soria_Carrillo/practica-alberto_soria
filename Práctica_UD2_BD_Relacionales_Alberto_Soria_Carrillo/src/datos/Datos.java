package datos;

/**
 * Clase Datos. guarda toda la información necesaria para la configuración
 *
 * @author Alberto Soria Carrillo
 */
public class Datos {
    //Atributos
    private final String config = "config.txt";
    private String ipServer = "";
    private String puertoServer = "";
    private String nombreBBDD = "";
    private String usurarioBBDD = "";
    private String paswordUsuario = "";
    private boolean controlVistaConfig = true;
    // Getters and Setters
    public String getConfig() {
        return config;
    }

    public String getIpServer() {
        return ipServer;
    }

    public void setIpServer(String ipServer) {
        this.ipServer = ipServer;
    }

    public String getPuertoServer() {
        return puertoServer;
    }

    public void setPuertoServer(String puertoServer) {
        this.puertoServer = puertoServer;
    }

    public String getNombreBBDD() {
        return nombreBBDD;
    }

    public void setNombreBBDD(String nombreBBDD) {
        this.nombreBBDD = nombreBBDD;
    }

    public String getUsurarioBBDD() {
        return usurarioBBDD;
    }

    public void setUsurarioBBDD(String usurarioBBDD) {
        this.usurarioBBDD = usurarioBBDD;
    }

    public String getPaswordUsuario() {
        return paswordUsuario;
    }

    public void setPaswordUsuario(String paswordUsuario) {
        this.paswordUsuario = paswordUsuario;
    }

    public boolean isControlVistaConfig() {
        return controlVistaConfig;
    }

    public void setControlVistaConfig(boolean controlVistaConfig) {
        this.controlVistaConfig = controlVistaConfig;
    }
}
