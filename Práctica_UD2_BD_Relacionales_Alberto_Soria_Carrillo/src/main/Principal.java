package main;

import ventanas.VistaConfig;

/**
 * Clase Principal. contiene el main del programa
 *
 * @author Alberto Soria Carrillo
 */
public class Principal {
    /**
     * Inicializa el programa
     * @param args
     */
    public static void main(String[] args) {
        VistaConfig vistaConfig = new VistaConfig();
    }

}