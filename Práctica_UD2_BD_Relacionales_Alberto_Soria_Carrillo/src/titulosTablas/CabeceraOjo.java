package titulosTablas;

/**
 * Clase CabeceraOjo, guarda la informacion de la cabecera la las tablas datosOjoIzqFacturaTable,
 * datosOjoDrchFacturaTable,listaOjoIzquierdoTable,listaOjoDerechaTable,ojoIzquierdoTable,ojoDerechaTable
 *
 * @author Alberto Soria Carrillo
 */
public enum CabeceraOjo {

    ID("Id"),
    HIPERMETROPIA("Hipermetropia"),
    ASTIGMATISMO("Astigmatismo"),
    REDUCIDO("Reducido"),
    GRADUACION("Graduación"),
    TIPO("Tipo");

    // Atributos
    private String string;

    // Getters and Setters
    public String getString() {
        return string;
    }

    // Constructor
    CabeceraOjo(String string) {

        this.string = string;

    }

}
