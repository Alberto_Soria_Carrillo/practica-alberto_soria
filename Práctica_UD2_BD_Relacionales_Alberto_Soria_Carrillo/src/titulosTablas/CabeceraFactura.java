package titulosTablas;

/**
 * Clase CabeceraFactura, que contiene las Cabeceras de las tablas factura
 * @author Alberto Soria Carrillo
 */
public enum CabeceraFactura {

    ID ("Id"),
    PRECIO_TOTAL ("Precio Total"),
    FECHA("Fecha"),
    NAME_OPTICA("Nombre Óptica"),
    DIRECCION("Dirección"),
    TELEFONO("Teléfono"),
    ID_CLIENTE("Id del Cliente");

    // Atributos
    private String string;

    // Getters and Setters
    public String getString() {
        return string;
    }

    // Constructor
    CabeceraFactura(String string){

        this.string = string;

    }

}
