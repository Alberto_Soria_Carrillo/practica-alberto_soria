package titulosTablas;

/**
 * Clase InfoOptica, guarda la informacion de la optica
 * @author Alberto Soria Carrillo
 */
public enum InfoOptica {
    NOMBRE("Opticas Gutierrez"),
    DIRECCION("Calle los Ciegos"),
    TELEFONO("976 86 24 85");

    // Atributos
    private String string;

    // Getters and Setters
    public String getString() {
        return string;
    }

    // Constructor
    InfoOptica(String string) {
        this.string = string;
    }

}
