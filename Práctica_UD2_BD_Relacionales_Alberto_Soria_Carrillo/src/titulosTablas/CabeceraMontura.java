package titulosTablas;

/**
 * Clase CabeceraMontura que guarda las cabeceras de las tablas monturaDtm y datosMonturaFacturaDmt
 * @author Alberto Soria Carrillo
 */
public enum CabeceraMontura {

    ID ("Id"),
    MARCA ("Marca"),
    MODELO ("Modelo"),
    PRECIO ("Precio");

    // Atributos
    private String string;

    // Getters and Setters
    public String getString() {
        return string;
    }

    // Constructor
    CabeceraMontura(String string){

        this.string = string;

    }
}
