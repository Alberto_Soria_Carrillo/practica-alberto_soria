package titulosTablas;

/**
 * Clase CabeceraCliente que contiene las cabeceras de las tablas clienteDtm y datosClienteFacturaDtm
 * @author Alberto Soria Carrillo
 */
public enum CabeceraCliente {

    ID ("Id"),
    NOMBRE ("Nombre"),
    APELLIDO1 ("1º Apellido"),
    APELLIDO2 ("2º Apellido"),
    DIRECCION ("Direccion"),
    GENERO ("Genero"),
    FECHA_NACIMIENTO ("Fecha de Nacimiento");

    // Atributos
    private String string;

    // Getters and Setters
    public String getString() {
        return string;
    }

    // Constructor
    CabeceraCliente(String string){

        this.string = string;

    }

}
