package conexion;

import datos.Datos;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 * Clase Conexion. Encargada de establecer la conexion con la base de datos
 *
 * @author Alberto Soria Carrillo
 */
public class Conexion {

    // Atributos
    private Datos dato; // Guarda la informcaion del programa
    private Connection connection;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;
    private CallableStatement callableStatement; // Para invocar procedimientos

    // Getters and Setters
    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public void setResultSet(ResultSet resultSet) {
        this.resultSet = resultSet;
    }

    public PreparedStatement getPreparedStatement() {
        return preparedStatement;
    }

    public void setPreparedStatement(PreparedStatement statement) {
        this.preparedStatement = statement;
    }

    public ResultSet getResultSet() {
        return resultSet;
    }

    public CallableStatement getCallableStatement() {
        return callableStatement;
    }

    public void setCallableStatement(CallableStatement callableStatement) {
        this.callableStatement = callableStatement;
    }

    // Constructor
    public Conexion(Datos dato) {
        this.dato = dato;
    }

    /**
     * Encargado de la conexion
     */
    public void conectar() {
        try {
            String url = "jdbc:mysql://" + dato.getIpServer() + ":" + dato.getPuertoServer() + "/" + dato.getNombreBBDD()
                    + "?serverTimezone=UTC";

            Class.forName("com.mysql.cj.jdbc.Driver"); // controlador nuevo
            // Class.forName("com.mysql.jdbc.Driver"); //controlador antiguo deprecado

            this.connection = DriverManager.getConnection(url, dato.getUsurarioBBDD(), dato.getPaswordUsuario());

        } catch (SQLException | ClassNotFoundException e) {

            createBBDD();

            //JOptionPane.showMessageDialog(null, "Fallo en la conexión");
            //e.printStackTrace();
        }
    }

    private void createBBDD() {
        try {
            String url = "jdbc:mysql://" + dato.getIpServer() + ":" + dato.getPuertoServer() + "/?serverTimezone=UTC";

            Class.forName("com.mysql.cj.jdbc.Driver"); // controlador nuevo
            // Class.forName("com.mysql.jdbc.Driver"); //controlador antiguo deprecado


            this.connection = DriverManager.getConnection(url, dato.getUsurarioBBDD(), dato.getPaswordUsuario());

            BufferedReader br = new BufferedReader(new FileReader("SQL/BBDD_SQL.txt"));

            String BBDD = "";
            String linea;
            while ((linea=br.readLine())!=null){
                    BBDD = BBDD + linea;
            }

            BBDD.replaceAll("delimiter //", "");
            BBDD.replaceAll("end //", "end;");
            String[] query = BBDD.split("--");

            for (int i =0; i< query.length; i++){
                    insertarPS(this,query[i]);
            }

        } catch (SQLException | ClassNotFoundException | FileNotFoundException e) {

            JOptionPane.showMessageDialog(null, "Fallo en la conexión");
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    /**
     * Encargado de la desconexion
     */
    public void desconectar() {
        try {
            this.connection.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Fallo en la desconexión");
            e.printStackTrace();
        }
    }

    /**
     * Realiza la query sql indicada
     *
     * @param conexion
     */
    public ResultSet buscarPS(Conexion conexion, String query) throws SQLException {

        conexion.setPreparedStatement(conexion.getConnection().prepareStatement(query));
        conexion.setResultSet(conexion.getPreparedStatement().executeQuery());
        return conexion.getResultSet();
    }

    /**
     * Realiza la query sql indicada
     *
     * @param conexion
     * @param query
     * @throws SQLException
     */
    public ResultSet buscarCS(Conexion conexion, String query) throws SQLException {

        conexion.setCallableStatement(conexion.getConnection().prepareCall(query));
        conexion.setResultSet(conexion.getCallableStatement().executeQuery());

        return conexion.getResultSet();
    }

    /**
     * Realiza las peticions de inserción en la BBDD
     * @param conexion
     * @param query
     * @throws SQLException
     */
    public void insertarPS(Conexion conexion, String query) throws SQLException {
        conexion.setPreparedStatement(conexion.getConnection().prepareStatement(query));
        conexion.getPreparedStatement().executeUpdate();
    }

    /**
     * Realiza las peticion de eliminación de las BBDD
     * @param query
     * @param conexion
     * @throws SQLException
     */
    public void deletPS(String query, Conexion conexion) throws SQLException {
        conexion.setPreparedStatement(conexion.getConnection().prepareStatement(query));
        conexion.getPreparedStatement().execute();
    }

    /**
     * Realiza las peticion es actualizacón en la BBDD
     * @param query
     * @param conexion
     * @throws SQLException
     */
    public void updatePS(String query, Conexion conexion) throws SQLException {
        conexion.setPreparedStatement(conexion.getConnection().prepareStatement(query));
        conexion.getPreparedStatement().executeUpdate();
    }
}

